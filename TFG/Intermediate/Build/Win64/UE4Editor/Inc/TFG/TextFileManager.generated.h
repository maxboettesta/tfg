// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TFG_TextFileManager_generated_h
#error "TextFileManager.generated.h already included, missing '#pragma once' in TextFileManager.h"
#endif
#define TFG_TextFileManager_generated_h

#define TFG_Source_TFG_TextFileManager_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSaveArray) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SaveDirectory); \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_TARRAY(FString,Z_Param_SaveText); \
		P_GET_UBOOL(Z_Param_AllowOverWritting); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UTextFileManager::SaveArray(Z_Param_SaveDirectory,Z_Param_FileName,Z_Param_SaveText,Z_Param_AllowOverWritting); \
		P_NATIVE_END; \
	}


#define TFG_Source_TFG_TextFileManager_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSaveArray) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SaveDirectory); \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileName); \
		P_GET_TARRAY(FString,Z_Param_SaveText); \
		P_GET_UBOOL(Z_Param_AllowOverWritting); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=UTextFileManager::SaveArray(Z_Param_SaveDirectory,Z_Param_FileName,Z_Param_SaveText,Z_Param_AllowOverWritting); \
		P_NATIVE_END; \
	}


#define TFG_Source_TFG_TextFileManager_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTextFileManager(); \
	friend struct Z_Construct_UClass_UTextFileManager_Statics; \
public: \
	DECLARE_CLASS(UTextFileManager, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TFG"), NO_API) \
	DECLARE_SERIALIZER(UTextFileManager)


#define TFG_Source_TFG_TextFileManager_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUTextFileManager(); \
	friend struct Z_Construct_UClass_UTextFileManager_Statics; \
public: \
	DECLARE_CLASS(UTextFileManager, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TFG"), NO_API) \
	DECLARE_SERIALIZER(UTextFileManager)


#define TFG_Source_TFG_TextFileManager_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextFileManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextFileManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextFileManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextFileManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextFileManager(UTextFileManager&&); \
	NO_API UTextFileManager(const UTextFileManager&); \
public:


#define TFG_Source_TFG_TextFileManager_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextFileManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextFileManager(UTextFileManager&&); \
	NO_API UTextFileManager(const UTextFileManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextFileManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextFileManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextFileManager)


#define TFG_Source_TFG_TextFileManager_h_15_PRIVATE_PROPERTY_OFFSET
#define TFG_Source_TFG_TextFileManager_h_12_PROLOG
#define TFG_Source_TFG_TextFileManager_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TFG_Source_TFG_TextFileManager_h_15_PRIVATE_PROPERTY_OFFSET \
	TFG_Source_TFG_TextFileManager_h_15_RPC_WRAPPERS \
	TFG_Source_TFG_TextFileManager_h_15_INCLASS \
	TFG_Source_TFG_TextFileManager_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TFG_Source_TFG_TextFileManager_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TFG_Source_TFG_TextFileManager_h_15_PRIVATE_PROPERTY_OFFSET \
	TFG_Source_TFG_TextFileManager_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TFG_Source_TFG_TextFileManager_h_15_INCLASS_NO_PURE_DECLS \
	TFG_Source_TFG_TextFileManager_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TFG_API UClass* StaticClass<class UTextFileManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TFG_Source_TFG_TextFileManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
