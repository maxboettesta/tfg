// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TFG/TextFileManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTextFileManager() {}
// Cross Module References
	TFG_API UClass* Z_Construct_UClass_UTextFileManager_NoRegister();
	TFG_API UClass* Z_Construct_UClass_UTextFileManager();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_TFG();
	TFG_API UFunction* Z_Construct_UFunction_UTextFileManager_SaveArray();
// End Cross Module References
	void UTextFileManager::StaticRegisterNativesUTextFileManager()
	{
		UClass* Class = UTextFileManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SaveArray", &UTextFileManager::execSaveArray },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTextFileManager_SaveArray_Statics
	{
		struct TextFileManager_eventSaveArray_Parms
		{
			FString SaveDirectory;
			FString FileName;
			TArray<FString> SaveText;
			bool AllowOverWritting;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static void NewProp_AllowOverWritting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AllowOverWritting;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SaveText;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SaveText_Inner;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SaveDirectory;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TextFileManager_eventSaveArray_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextFileManager_eventSaveArray_Parms), &Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_AllowOverWritting_SetBit(void* Obj)
	{
		((TextFileManager_eventSaveArray_Parms*)Obj)->AllowOverWritting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_AllowOverWritting = { "AllowOverWritting", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TextFileManager_eventSaveArray_Parms), &Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_AllowOverWritting_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_SaveText = { "SaveText", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextFileManager_eventSaveArray_Parms, SaveText), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_SaveText_Inner = { "SaveText", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextFileManager_eventSaveArray_Parms, FileName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_SaveDirectory = { "SaveDirectory", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TextFileManager_eventSaveArray_Parms, SaveDirectory), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_AllowOverWritting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_SaveText,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_SaveText_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_FileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::NewProp_SaveDirectory,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Custom" },
		{ "Keywords", "Save" },
		{ "ModuleRelativePath", "TextFileManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTextFileManager, nullptr, "SaveArray", sizeof(TextFileManager_eventSaveArray_Parms), Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTextFileManager_SaveArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTextFileManager_SaveArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTextFileManager_NoRegister()
	{
		return UTextFileManager::StaticClass();
	}
	struct Z_Construct_UClass_UTextFileManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTextFileManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_TFG,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTextFileManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTextFileManager_SaveArray, "SaveArray" }, // 4174130018
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTextFileManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "TextFileManager.h" },
		{ "ModuleRelativePath", "TextFileManager.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTextFileManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTextFileManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTextFileManager_Statics::ClassParams = {
		&UTextFileManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTextFileManager_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UTextFileManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTextFileManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTextFileManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTextFileManager, 1399910902);
	template<> TFG_API UClass* StaticClass<UTextFileManager>()
	{
		return UTextFileManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTextFileManager(Z_Construct_UClass_UTextFileManager, &UTextFileManager::StaticClass, TEXT("/Script/TFG"), TEXT("UTextFileManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTextFileManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
